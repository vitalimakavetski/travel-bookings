package com.qualogy.travel;

import static org.apache.spark.sql.functions.window;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.streaming.OutputMode;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

public class TravelBookingsStreamingJob {

  private static final String BASE_DATA_PATH = "D:\\Vitali\\Docs\\assignments";
  private static final String CHECKPOINT_DIRECTORY_NAME = "checkpoint";
  private static final String DATA_DIRECTORY_NAME = "data";
  private static final String FILE_NAME = "booking_data.csv";
  private static final String ACCOMODATION_TYPE = "AcomType";
  private static final String TIMESTAMP = "Timestamp";

  public static void main(final String[] args) {
    System.setProperty("hadoop.home.dir", "C:\\winutils");

    final String sourceDirectory = Paths.get(BASE_DATA_PATH, DATA_DIRECTORY_NAME).toString();
    final SparkSession session = SparkSession.builder().appName("TravelBookingsStreamingJob")
        .master("local").config("spark.sql.shuffle.partitions", 4)
        .config("spark.sql.streaming.checkpointLocation",
            Paths.get(BASE_DATA_PATH, CHECKPOINT_DIRECTORY_NAME).toString())
        .getOrCreate();

    final StructType csvSchema =
        new StructType().add("id", DataTypes.StringType).add("durationOfStay", DataTypes.StringType)
            .add("gender", DataTypes.StringType).add("Age", DataTypes.StringType)
            .add("kids", DataTypes.StringType).add("destinationCode", DataTypes.StringType)
            .add(ACCOMODATION_TYPE, DataTypes.StringType).add(TIMESTAMP, DataTypes.StringType);

    final Dataset<Row> csvData =
        session.readStream().option("header", true).schema(csvSchema).csv(sourceDirectory);
    final Dataset<Row> csvDataTransformed = csvData.withColumn(TIMESTAMP,
        csvData.col(TIMESTAMP).cast(DataTypes.TimestampType).name(TIMESTAMP));
    final Dataset<Row> windowedData = csvDataTransformed.withWatermark(TIMESTAMP, "10 minutes")
        .groupBy(window(csvDataTransformed.col(TIMESTAMP), "1 hour", "10 minutes"),
            csvDataTransformed.col(ACCOMODATION_TYPE))
        .count().sortWithinPartitions("window");

    final StreamingQuery stream = windowedData.writeStream().format("console")
        .option("truncate", false).outputMode(OutputMode.Complete()).start();

    try {
      Files.copy(Paths.get(BASE_DATA_PATH, FILE_NAME), Paths.get(sourceDirectory, FILE_NAME),
          StandardCopyOption.REPLACE_EXISTING);
      stream.awaitTermination(20_000);
    } catch (final StreamingQueryException e) {
      e.printStackTrace();
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }
}
